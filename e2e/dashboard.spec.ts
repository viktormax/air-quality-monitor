import { test, expect } from '@playwright/test';

const NUMBER_OF_DATA_POINTS = 24 * 6; // Every 10 minutes in a day.

test.beforeEach(async ({ page }) => {
  await page.goto('/');
});

test.describe('dashboard', () => {
  test('has title', async ({ page }) => {
    await expect(page).toHaveTitle(/Air Quality Monitor/);
  });

  test('has light chart', async ({ page }) => {
    const chart = page.locator('#chart-light');

    await expect(chart.locator('p')).toHaveText('light');
    await expect(chart.locator('.recharts-bar-rectangles g')).toHaveCount(NUMBER_OF_DATA_POINTS);
  });

  test('has temperature chart', async ({ page }) => {
    const chart = page.locator('#chart-temperature');

    await expect(chart.locator('p')).toHaveText('temperature');
    const line = await chart.locator('.recharts-area-curve').getAttribute('d');
    expect(line?.split('L').length).toEqual(NUMBER_OF_DATA_POINTS);
  });

  test('has humidity chart', async ({ page }) => {
    const chart = page.locator('#chart-humidity');

    await expect(chart.locator('p')).toHaveText('humidity');
    const line = await chart.locator('.recharts-area-curve').getAttribute('d');
    expect(line?.split('L').length).toEqual(NUMBER_OF_DATA_POINTS);
  });

  test('has co2 chart', async ({ page }) => {
    const chart = page.locator('#chart-co2');

    await expect(chart.locator('p')).toHaveText('co2');
    const line = await chart.locator('.recharts-area-curve').getAttribute('d');
    expect(line?.split('L').length).toEqual(NUMBER_OF_DATA_POINTS);
  });
});
