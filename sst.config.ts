/* eslint-disable prefer-arrow-callback */
import { SSTConfig } from 'sst';
import { Config, NextjsSite } from 'sst/constructs';

export default {
  config(_input) {
    return {
      name: 'air-quality-next',
      region: 'eu-central-1',
    };
  },
  stacks(app) {
    // Pass ONLY named functions, otherwise AWS deploy fails.
    app.stack(function Site({ stack }) {
      const FIREBASE_CLIENT_EMAIL = new Config.Secret(stack, 'FIREBASE_CLIENT_EMAIL');
      const FIREBASE_PRIVATE_KEY = new Config.Secret(stack, 'FIREBASE_PRIVATE_KEY');
      const SECRETS = [FIREBASE_CLIENT_EMAIL, FIREBASE_PRIVATE_KEY];

      const site = new NextjsSite(stack, 'site', { bind: SECRETS });

      stack.addOutputs({
        SiteUrl: site.url,
      });
    });
  },
} satisfies SSTConfig;
