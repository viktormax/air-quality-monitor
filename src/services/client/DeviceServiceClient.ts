import { DeviceData } from '@/models/DeviceData';
import { Uuid } from '@/models/Uuid';
import { Unsubscribe, getDatabase, onValue, ref } from 'firebase/database';
import { getClientApp } from '@/database/firebase/getClientApp';

interface GetRealtimeData {
  deviceId: Uuid;
  onChange: (value: DeviceData) => void;
  onError: (error: Error) => void;
}

export class DeviceServiceClient {
  constructor(
    private app = getClientApp(),
    private db = getDatabase(app)
  ) {}

  watchRealtimeData({ deviceId, onChange, onError }: GetRealtimeData): Unsubscribe {
    const dataRef = ref(this.db, `devices/${deviceId}/data`);

    return onValue(
      dataRef,
      (snapshot) => onChange(DeviceData.parse(snapshot.val())),
      (error) => onError(error)
    );
  }
}
