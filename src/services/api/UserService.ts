import { UserRepository } from '@/database/firebase/UserRepository';
import { CreateUser } from '@/models/User';
import { z } from 'zod';

export class UserService {
  constructor(private userRepo = new UserRepository()) {}

  async signUp(props: z.infer<typeof CreateUser>) {
    const createUser = CreateUser.parse(props);
    return this.userRepo.createUser(createUser);
  }
}
