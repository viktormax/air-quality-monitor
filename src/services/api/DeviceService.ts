import { z } from 'zod';
import { Uuid } from '@/models/Uuid';
import { TimePeriod } from '@/models/TimePeriod';
import { filterByDate } from '@/utils/filters';
import { CreateDeviceData } from '@/models/DeviceData';
import { Command } from '@/models/Command';
import { dropElementsToSize } from '@/utils/helpers';
import { MAX_DEVICE_JSON_BYTES_SIZE } from '@/utils/constants';
import moment from '@/lib/moment';
import { DeviceRepository } from '@/database';

const GetDeviceData = z.object({
  deviceId: Uuid,
  period: TimePeriod.optional(),
});

const StoreDeviceData = z.object({
  deviceId: Uuid,
  data: CreateDeviceData,
});

const GetDevicePendingCommands = z.object({
  deviceId: Uuid,
});

const GetDeviceCommands = z.object({
  deviceId: Uuid,
});

const GetRealtimeData = z.object({
  deviceId: Uuid,
});

export class DeviceService {
  constructor(private deviceRepo = new DeviceRepository()) {}

  async getDeviceData(props: z.infer<typeof GetDeviceData>) {
    const { period, deviceId } = GetDeviceData.parse(props);

    if (!period) {
      return [];
    }

    const data = await this.deviceRepo.getDeviceData(deviceId, period);

    return data.filter(filterByDate(period.from, period.to, 'minutes'));
  }

  async getRealtimeData(props: z.infer<typeof GetRealtimeData>) {
    const { deviceId } = GetRealtimeData.parse(props);
    return this.deviceRepo.getRealTimeData(deviceId);
  }

  async storeDeviceData(props: z.infer<typeof StoreDeviceData>) {
    const { data, deviceId } = StoreDeviceData.parse(props);

    const now = moment.utc();
    const midnight = now.clone().startOf('day');
    const day = now.format('YYYY-MM-DD');
    const minute = String(Math.floor(now.diff(midnight, 'minutes') / 10));

    return Promise.all([
      this.deviceRepo.storeRealTimeData(deviceId, data),
      this.deviceRepo.storeDeviceData(deviceId, day, minute, data),
    ]);
  }

  async getDevicePendingCommands(props: z.infer<typeof GetDevicePendingCommands>): Promise<Command[]> {
    const { deviceId } = GetDevicePendingCommands.parse(props);

    const commands = await this.deviceRepo.getDeviceUnfinishedCommands(deviceId);

    const filtered = commands.filter((x) => x.status === 'pending');

    return dropElementsToSize(filtered, MAX_DEVICE_JSON_BYTES_SIZE);
  }

  async getDeviceCommands(props: z.infer<typeof GetDeviceCommands>): Promise<Command[]> {
    const { deviceId } = GetDeviceCommands.parse(props);

    const unfinished = await this.deviceRepo.getDeviceUnfinishedCommands(deviceId);
    const dev = await this.deviceRepo.getDevice(deviceId);

    return unfinished.concat(dev?.commands ?? []);
  }
}
