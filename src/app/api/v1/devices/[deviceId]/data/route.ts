import { Command } from '@/models/Command';
import { DeviceService } from '@/services/api';
import { errorHandler } from '@/utils/errorHandler';
import { NextRequest, NextResponse } from 'next/server';

interface Params {
  params: {
    deviceId: string;
  };
}

export async function POST(req: NextRequest, { params }: Params): Promise<NextResponse<Command[]>> {
  const deviceService = new DeviceService();

  try {
    const [commands] = await Promise.all([
      deviceService.getDevicePendingCommands(params),
      deviceService.storeDeviceData({
        deviceId: params.deviceId,
        data: await req.json(),
      }),
    ]);
    return NextResponse.json(commands);
  } catch (err) {
    return errorHandler(req, err);
  }
}
