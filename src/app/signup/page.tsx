'use client';

import { NextPage } from 'next';
import React from 'react';
import { useForm } from 'react-hook-form';
import { Card, TextInput, Button, Text } from '@tremor/react';
import { CreateUser } from '@/models/User';
import { zodResolver } from '@hookform/resolvers/zod';
import { toast } from 'react-toastify';
import { useRouter } from 'next/navigation';
import { ErrorCode } from '@/utils/AppError';
import { signUp } from './actions';

const Page: NextPage = () => {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<CreateUser>({
    resolver: zodResolver(CreateUser),
  });

  const onSignUp = async (data: CreateUser) => {
    const { error } = await signUp(data);

    if (error) {
      if (error.code === ErrorCode.EmailAlreadyInUse) {
        setError('email', { message: error.message });
        return;
      }

      toast.success('Something went wrong.');
      return;
    }

    toast.success('User is created.');
    router.push('/');
  };

  return (
    <div className="flex min-h-screen items-center justify-center bg-gray-100">
      <Card className="w-full max-w-md rounded-md bg-white p-8 shadow-md">
        <Text className="mb-8 text-center text-2xl font-bold">Sign Up</Text>
        <form onSubmit={handleSubmit(onSignUp)} className="flex flex-col gap-6">
          <div>
            <TextInput
              error={!!errors.email?.message}
              errorMessage={errors.email?.message}
              type="email"
              placeholder="Email"
              {...register('email')}
            />
          </div>
          <TextInput type="password" placeholder="Password" {...register('password')} />
          <Button type="submit" className="w-full rounded bg-blue-500 px-4 py-2 font-bold text-white hover:bg-blue-700">
            Sign Up
          </Button>
        </form>
      </Card>
    </div>
  );
};

export default Page;
