'use server';

import { formActionResult } from '@/utils/ActionResult';
import { CreateUser } from '@/models/User';
import { UserService } from '@/services/api/UserService';

export const signUp = async (props: CreateUser) => formActionResult(() => new UserService().signUp(props));
