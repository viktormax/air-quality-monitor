import { BarChart } from '@tremor/react';
import { NextPage } from 'next';
import {
  mapDeviceDataToLineChartData,
  mapDeviceDataToLocalizedData,
  mapDeviceDataToUptimeData,
  mapSearchParamsToTimePeriod,
} from '@/utils/mappers';
import { DeviceService } from '@/services/api';
import DatePicker from '@/components/DatePicker';
import Uptime from '@/components/DeviceUptime';
import Chart from '@/components/Chart';
import { cookies } from 'next/headers';
import { CookieStorage } from '@/utils/CookieStorage';
import { DEVICE_ID } from '@/utils/constants';
import RealtimeData from './RealtimeData';

export interface IPageProps {
  searchParams: Record<string, string | undefined>;
}

const Page: NextPage<IPageProps> = async ({ searchParams }) => {
  const deviceService = new DeviceService();
  const tzName = CookieStorage.tzName.get(cookies());
  const { period } = mapSearchParamsToTimePeriod(searchParams, tzName);
  const data = await deviceService.getDeviceData({
    deviceId: DEVICE_ID,
    period,
  });
  const realtimeData = await deviceService.getRealtimeData({ deviceId: DEVICE_ID });
  const localizedData = mapDeviceDataToLocalizedData(data, tzName);
  const chartData = mapDeviceDataToLineChartData(localizedData);
  const uptimeData = mapDeviceDataToUptimeData(localizedData);

  return (
    <main className="flex min-h-screen flex-col items-center justify-between py-6">
      <RealtimeData data={realtimeData} />
      <Uptime data={uptimeData} className="w-1/2" />
      <DatePicker className="my-8" />
      <div className="mt-4 flex w-full flex-col gap-8 pr-8">
        <Chart data={chartData} index="date" category="light" color="amber" ChartComponent={BarChart} />
        <Chart data={chartData} index="date" category="temperature" color="lime" />
        <Chart data={chartData} index="date" category="humidity" color="sky" />
        <Chart data={chartData} index="date" category="co2" color="orange" />
      </div>
    </main>
  );
};

export default Page;
