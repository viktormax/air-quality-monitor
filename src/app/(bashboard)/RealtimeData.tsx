'use client';

import GaugeChart from '@/components/GaugeChart';
import useRealTimeData from '@/hooks/useRealTimeData';
import { DeviceData } from '@/models/DeviceData';
import { DEVICE_ID, ONLINE_STATUS_MAX_MINUTES } from '@/utils/constants';
import moment from 'moment';

interface Props {
  data?: DeviceData;
}

const RealtimeData: React.FC<Props> = ({ data }) => {
  const realtime = useRealTimeData(DEVICE_ID) ?? data;
  const offline = !realtime || moment().diff(realtime.date, 'minute') > ONLINE_STATUS_MAX_MINUTES;

  return (
    <div className="flex flex-wrap justify-center">
      <GaugeChart
        data={realtime?.temperature}
        offline={offline}
        min={15}
        max={35}
        unit="°C"
        graduationType="dot"
        colorFilledMin="#0ea5e9"
        colorFilledMax="#ef4444"
      />
      <GaugeChart data={realtime?.humidity} offline={offline} unit="%" />
    </div>
  );
};

export default RealtimeData;
