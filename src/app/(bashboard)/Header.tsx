import Link from 'next/link';
import { CookieStorage } from '@/utils/CookieStorage';
import { cookies } from 'next/headers';
import TimeZoneSelect from './TimeZoneSelect';

interface Props {}

const Header: React.FC<Props> = () => {
  const tzName = CookieStorage.tzName.get(cookies());

  return (
    <header className="grid grid-cols-3">
      <div />
      <div className="flex h-16 items-center justify-center gap-6">
        <Link className="" href="/">
          Dashboard
        </Link>
        {/* <Link className="" href="/commands">
          Commands
        </Link> */}
      </div>
      <div className="flex items-center justify-end px-8">
        <TimeZoneSelect initTzName={tzName} />
      </div>
    </header>
  );
};

export default Header;
