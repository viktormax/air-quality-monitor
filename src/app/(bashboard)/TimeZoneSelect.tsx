'use client';

import useDidMountEffect from '@/hooks/useDidMountEffect';
import { CookieStorage } from '@/utils/CookieStorage';
import { SearchSelect, SearchSelectItem } from '@tremor/react';
import moment from '@/lib/moment';
import { useState } from 'react';
import timezonesRaw from 'timezones.json';
import { useRouter } from 'next/navigation';

const getDefaultTimezone = (tzName?: string) => {
  const timeZone = tzName || CookieStorage.tzName.get() || Intl.DateTimeFormat().resolvedOptions().timeZone;
  return timezonesRaw.find((x) => x.utc.includes(timeZone))?.value;
};

const formatTimeZoneTest = (tzName: string, timeZoneString: string): string =>
  timeZoneString.replace(/\(UTC[0-9+-:]*\)/, moment().tz(tzName).format('(UTCZ)'));

const timezones = new Map(
  timezonesRaw
    .filter((x) => x.utc.length > 0)
    .map((x) => [x.value, { text: formatTimeZoneTest(x.utc[0], x.text), utc: x.utc[0] }])
);

interface Props {
  initTzName?: string;
}

const TimeZoneSelect: React.FC<Props> = ({ initTzName }) => {
  const [timezone, setTimezone] = useState(getDefaultTimezone(initTzName));
  const router = useRouter();

  const onChange = (tz: string | undefined) => {
    setTimezone(tz);

    const tzName = tz && timezones.get(tz)?.utc;
    if (tzName !== CookieStorage.tzName.get()) {
      CookieStorage.tzName.set(tzName);
      router.refresh();
    }
  };

  useDidMountEffect(() => {
    onChange(getDefaultTimezone());
  });

  return (
    <div className="w-72">
      <SearchSelect onValueChange={onChange} value={timezone} placeholder="Please select timezone.">
        {Array.from(timezones).map(([value, tz]) => (
          <SearchSelectItem key={value} value={value}>
            {tz.text}
          </SearchSelectItem>
        ))}
      </SearchSelect>
    </div>
  );
};

export default TimeZoneSelect;
