import moment from 'moment-timezone';

export { type Moment } from 'moment-timezone';

export default moment;
