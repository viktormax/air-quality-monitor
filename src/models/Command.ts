import { z } from 'zod';
import { ZodMoment } from './ZodMoment';

export const CommandType = z.enum(['auto-calibration-on', 'auto-calibration-off', 'zero-calibration']);

export type CommandType = z.infer<typeof CommandType>;

export const CommandStatus = z.enum(['pending', 'scheduled', 'executed', 'canceled']);

export type CommandStatus = z.infer<typeof CommandStatus>;

export const Command = z.object({
  id: z.string(),
  type: CommandType,
  status: CommandStatus,
  date: ZodMoment,
});

export type Command = z.infer<typeof Command>;
