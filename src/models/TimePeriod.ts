import { z } from 'zod';
import { ZodMoment } from './ZodMoment';

export const TimePeriod = z.object({
  from: ZodMoment.optional(),
  to: ZodMoment.optional(),
});

export type TimePeriod = z.infer<typeof TimePeriod>;
