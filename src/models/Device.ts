import { z } from 'zod';
import { Command } from './Command';

export const Device = z.object({
  id: z.string().uuid(),
  commands: z.array(Command),
});

export type Device = z.infer<typeof Device>;
