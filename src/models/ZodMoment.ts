/* eslint-disable no-underscore-dangle */
import moment, { Moment } from '@/lib/moment';
import { z } from 'zod';

const tryParse = (value: any): Moment => {
  if (value == null || typeof value !== 'object') {
    return moment(value ?? null);
  }

  const seconds = value.seconds ?? value._seconds;
  const nanoseconds = value.nanoseconds ?? value._nanoseconds;

  // Handle firebase timestamp as UTC.
  if (seconds != null && nanoseconds != null) {
    const milliseconds = seconds * 1000 + nanoseconds / 1000000;
    return moment.utc(milliseconds);
  }

  return moment(value);
};

export const ZodMoment = z
  .custom<Moment>((value) => tryParse(value).isValid(), {
    message: 'Invalid date format',
  })
  .transform((value) => tryParse(value));
