import { z } from 'zod';

export const User = z.object({
  uid: z.string(),
  email: z.string(),
});

export const CreateUser = z.object({
  email: z.string().email(),
  password: z.string(),
});

export type CreateUser = z.infer<typeof CreateUser>;

export type User = z.infer<typeof User>;
