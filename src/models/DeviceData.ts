import { z } from 'zod';
import { ZodMoment } from './ZodMoment';

export const DeviceData = z.object({
  humidity: z.number().min(0).max(100),
  temperature: z.number().min(-273).max(100),
  light: z.number().nonnegative().safe(),
  co2: z.number().min(0).max(5000),
  date: ZodMoment,
});

export type DeviceData = z.infer<typeof DeviceData>;

export const CreateDeviceData = DeviceData.extend({
  date: DeviceData.shape.date.optional(),
});

export type CreateDeviceData = z.infer<typeof CreateDeviceData>;
