import { memo } from 'react';

interface Props {
  text?: string | number;
  x?: number;
  y?: number;
  fill?: string;
  fontSize?: number;
  align?: 'middle';
  verticalAlign?: 'middle';
  offsetX?: number;
  offsetY?: number;
  offline?: boolean;
  superscript?: string;
}

const Text: React.FC<Props> = ({
  text,
  fill,
  fontSize = 30,
  align,
  verticalAlign,
  x,
  y,
  offsetX = 0,
  offsetY = 0,
  offline,
  superscript,
}) => (
  <text
    x={x}
    y={y}
    fill={fill}
    fontSize={fontSize}
    textAnchor={align}
    dominantBaseline={verticalAlign}
    transform={offline ? undefined : `translate(${-offsetX}, ${-offsetY})`}
  >
    {offline ? 'Off' : text}
    {!offline && superscript && (
      <tspan fontSize={fontSize / 2.5} baselineShift="super">
        {superscript}
      </tspan>
    )}
  </text>
);

export default memo(Text);
