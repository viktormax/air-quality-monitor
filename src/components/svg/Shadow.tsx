interface Props {
  x: number;
  y: number;
  blur: number;
  color: string;
}

const Shadow: React.FC<Props> = ({ x, y, color, blur }) => (
  <feDropShadow dx={x} dy={y} stdDeviation={blur / 2} floodColor={color} />
);

export default Shadow;
