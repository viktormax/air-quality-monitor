import { memo } from 'react';

interface Props {
  id?: string;
  height?: number;
  width?: number;
  x?: number;
  y?: number;
  rotation?: number;
  fill?: string;
  cornerRadius?: number;
}

const Rect: React.FC<Props> = ({ id, x = 0, y = 0, rotation, height, width, fill, cornerRadius }) => (
  <rect
    id={id}
    x={x}
    y={y}
    transform={rotation ? `rotate(${rotation}, ${x}, ${y})` : undefined}
    height={height}
    width={width}
    fill={fill}
    rx={cornerRadius}
    ry={cornerRadius}
  />
);

export default memo(Rect);
