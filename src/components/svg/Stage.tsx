import { memo } from 'react';

interface Props {
  height?: number;
  width?: number;
  children: React.ReactNode;
}

const Stage: React.FC<Props> = ({ height = 0, width = 0, children }) => (
  <svg width={width} height={width} viewBox={`0 0 ${height} ${width}`}>
    {children}
  </svg>
);

export default memo(Stage);
