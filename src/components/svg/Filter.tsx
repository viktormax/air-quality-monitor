interface Props {
  id: string;
  children: React.ReactNode;
}

const Filter: React.FC<Props> = ({ id, children }) => (
  <filter id={id} x="-50%" y="-50%" width="200%" height="200%">
    {children}
  </filter>
);

export default Filter;
