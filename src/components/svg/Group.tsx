import { memo } from 'react';

interface Props {
  id?: string;
  x?: number;
  y?: number;
  offsetX?: number;
  offsetY?: number;
  rotation?: number;
  children: React.ReactNode;
}

const Group: React.FC<Props> = ({ id, children, x = 0, y = 0, rotation = 0, offsetX = 0, offsetY = 0 }) => (
  <g id={id} transform={`translate(${x - offsetX}, ${y - offsetY}) rotate(${rotation}, ${x}, ${y})`}>
    {children}
  </g>
);

export default memo(Group);
