import { memo } from 'react';

interface Props {
  x?: number;
  y?: number;
  radius?: number;
  fill?: string;
  filterId?: string;
}

const Circle: React.FC<Props> = ({ x, y, radius, fill, filterId }) => (
  <circle x={x} y={y} r={radius} fill={fill} filter={filterId && `url(#${filterId})`} />
);

export default memo(Circle);
