'use client';

import { DeviceData } from '@/models/DeviceData';
import { AreaChart, Badge, BarChart } from '@tremor/react';
import { useEffect, useState } from 'react';
import _ from 'lodash';

interface ChartData extends Omit<DeviceData, 'date'> {
  date: string;
}

interface Props {
  className?: string;
  index: keyof ChartData;
  category: keyof ChartData;
  data: ChartData[];
  color: string;
  ChartComponent?: typeof BarChart | typeof AreaChart;
}

const Chart: React.FC<Props> = ({ index, className, data, category, color, ChartComponent = AreaChart }) => {
  const [min, setMin] = useState<number | string | undefined>(undefined);
  const [mean, setMean] = useState<number | string | undefined>(undefined);
  const [max, setMax] = useState<number | string | undefined>(undefined);

  useEffect(() => {
    if (data.length === 0) {
      return;
    }

    const maxData = _.maxBy(data, (x) => x[category]);
    const minData = _.minBy(data, (x) => x[category]);
    const meanData = _.meanBy(data, (x) => x[category]);

    setMin(minData && minData[category]);
    setMean(meanData && _.round(meanData, 2));
    setMax(maxData && maxData[category]);
  }, [category, data]);

  return (
    <div className={className}>
      <div className="flex justify-end gap-2">
        <Badge color="neutral">Min: {min}</Badge>
        <Badge color="neutral">Mean: {mean}</Badge>
        <Badge color="neutral">Max: {max}</Badge>
      </div>
      <ChartComponent
        className="size-full"
        id={`chart-${category}`}
        data={data}
        index={index}
        categories={[category]}
        colors={[color]}
        yAxisWidth={60}
        autoMinValue
      />
    </div>
  );
};

export default Chart;
