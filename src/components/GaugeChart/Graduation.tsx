import { memo } from 'react';
import { Rect } from '@/components/svg';
import _ from 'lodash';

export const getAngleRad = (value: number, range: number) => (4 * Math.PI * value) / (3 * (range - 1));

interface Props {
  index: number;
  range: number;
  r: number;
  cx: number;
  cy: number;
  filled: boolean;
  color: string;
  type: 'rect' | 'dot';
}

const Graduation: React.FC<Props> = ({ index, r, cy, cx, range, filled, color, type }) => {
  const angle = getAngleRad(index, range);

  if (type === 'dot') {
    return (
      <Rect
        x={_.round(cx + (r - (filled ? 5 : 4)) * Math.cos(angle), 2)}
        y={_.round(cy + (r - (filled ? 5 : 4)) * Math.sin(angle), 2)}
        height={filled ? 7 : 5}
        width={filled ? 7 : 5}
        fill={filled ? color : '#73737321'}
        cornerRadius={4}
      />
    );
  }

  return (
    <Rect
      x={_.round(cx + r * Math.cos(angle), 2)}
      y={_.round(cy + r * Math.sin(angle), 2)}
      rotation={_.round((angle * 180) / Math.PI + 90, 2)}
      height={filled ? 14 : 12}
      width={filled ? 4 : 3}
      fill={filled ? color : '#73737321'}
      cornerRadius={2}
    />
  );
};

export default memo(Graduation);
