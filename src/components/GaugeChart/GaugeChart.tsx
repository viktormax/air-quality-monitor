'use client';

import React, { memo, useId, useMemo } from 'react';
import useAnimatedValue from '@/hooks/useAnimatedValue';
import { Stage, Group, Circle, Text, Shadow, Filter } from '@/components/svg';
import _ from 'lodash';
import { interpolateLab } from 'd3-interpolate';
import Graduation from './Graduation';
import ValueDot from './ValueDot';

const color = {
  text: '#333333',
  filled: '#0ea5e9',
  shadow: '#C7C7C7',
  light: '#FFFFFF',
  circle: '#F1F1F1',
};

interface Props {
  size?: number;
  data?: number;
  min?: number;
  max?: number;
  range?: number;
  padding?: number;
  offline?: boolean;
  unit: string;
  graduationType?: 'dot' | 'rect';
  colorFilledMin?: string;
  colorFilledMax?: string;
}

const GaugeChart: React.FC<Props> = ({
  size = 350,
  padding = 20,
  data,
  min = 1,
  max = 100,
  range = max - min + 1,
  unit,
  graduationType = 'rect',
  offline = false,
  colorFilledMin = color.filled,
  colorFilledMax = colorFilledMin,
}) => {
  const value = useAnimatedValue(_.round(offline || !data ? min - 1 : data, 1), 10, 1, min - 1);
  const bars = useMemo(() => _.range(min, range + min), [min, range]);
  const circleFilterId = useId();

  const r = size / 2 - padding;
  const cy = size / 2;
  const cx = size / 2;

  return (
    <Stage width={size} height={size}>
      <defs>
        <Filter id={circleFilterId}>
          <Shadow x={-20} y={-20} blur={40} color={color.light} />
          <Shadow x={20} y={20} blur={40} color={color.shadow} />
        </Filter>
      </defs>
      <Group x={cx} y={cy}>
        <Circle radius={r - 30} fill={color.circle} filterId={circleFilterId} />
        <Text
          offsetX={-10}
          text={value}
          fill={color.text}
          fontSize={50}
          align="middle"
          verticalAlign="middle"
          superscript={unit}
          offline={offline}
        />
      </Group>
      <Group rotation={150} offsetX={cx} offsetY={cy} x={cx} y={cy}>
        <ValueDot
          color={interpolateLab(colorFilledMin, colorFilledMax)((value - min) / range)}
          cx={cx}
          cy={cy}
          r={r}
          range={range}
          value={value - min}
          offline={offline}
        />
        {bars.map((v, i) => (
          <Graduation
            key={v}
            index={i}
            range={range}
            cx={cx}
            cy={cy}
            r={r}
            filled={!offline && Math.floor(value) >= v}
            color={interpolateLab(colorFilledMin, colorFilledMax)((v - min) / range)}
            type={graduationType}
          />
        ))}
      </Group>
      <Text
        x={_.round(cx + (r - 5) * Math.cos(0.77 * Math.PI), 2)}
        y={_.round(cy + (r - 5) * Math.sin(0.77 * Math.PI), 2)}
        text={min}
        fill={color.text}
        fontSize={20}
        align="middle"
        verticalAlign="middle"
      />
      <Text
        x={_.round(cx + (r - 5) * Math.cos(0.23 * Math.PI), 2)}
        y={_.round(cy + (r - 5) * Math.sin(0.23 * Math.PI), 2)}
        text={max}
        fill={color.text}
        fontSize={20}
        align="middle"
        verticalAlign="middle"
      />
    </Stage>
  );
};

export default memo(GaugeChart);
