import { Circle, Group } from '@/components/svg';
import _ from 'lodash';
import { getAngleRad } from './Graduation';

interface Props {
  cx: number;
  cy: number;
  r: number;
  value: number;
  range: number;
  color: string;
  offset?: number;
  offline: boolean;
}

const ValueDot: React.FC<Props> = ({ r, range, value, color, cy, cx, offset = 45, offline }) => {
  if (value < 0 || offline) {
    return null;
  }

  const angle = getAngleRad(value, range);

  return (
    <Group x={_.round(cx + (r - offset) * Math.cos(angle), 2)} y={_.round(cy + (r - offset) * Math.sin(angle), 2)}>
      <Circle radius={3} fill={color} />
    </Group>
  );
};

export default ValueDot;
