'use client';

import { DatePickerPeriod } from '@/utils/typeguards';
import { DateRangePicker, DateRangePickerValue } from '@tremor/react';
import moment from '@/lib/moment';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';

const selectedValueToParams = new Map<string, DatePickerPeriod>([
  ['tdy', 'day'],
  ['w', 'week'],
  ['t', 'month'],
]);

interface Props {
  className?: string;
}

const DatePicker: React.FC<Props> = ({ className }) => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace } = useRouter();

  const onChange = (value: DateRangePickerValue) => {
    const params = new URLSearchParams(searchParams);

    if (value.from) {
      params.set('from', moment.utc(value.from).startOf('day').format());
    } else {
      params.delete('from');
    }

    if (value.to) {
      params.set('to', moment.utc(value.to).add(1, 'day').format());
    } else {
      params.delete('to');
    }

    if (value.selectValue) {
      const last = selectedValueToParams.get(value.selectValue);
      params.set('last', last ?? value.selectValue);
      params.delete('from');
      params.delete('to');
    }

    replace(`${pathname}?${params.toString()}`);
  };

  return (
    <div className={`${className} flex flex-col`}>
      <p className="mb-1 text-center font-mono text-sm text-slate-500">Date Range Picker</p>
      <DateRangePicker className="mx-auto max-w-md" onValueChange={onChange} />
    </div>
  );
};

export default DatePicker;
