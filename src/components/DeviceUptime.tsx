import { Tracker } from '@tremor/react';

export interface UptimeBlockProps {
  key?: string | number;
  color?: string;
  tooltip: string;
}

interface Props {
  className: string;
  data: UptimeBlockProps[];
}

const Uptime: React.FC<Props> = ({ data, className }) => (
  <div className={className}>
    <p className="mb-1 text-center font-mono text-sm text-slate-500">Device Uptime</p>
    <Tracker data={data} />
  </div>
);

export default Uptime;
