import { NextRequest, NextResponse } from 'next/server';
import { z } from 'zod';

export const errorHandler = (req: NextRequest, error: any): NextResponse<any> => {
  if (error instanceof z.ZodError) {
    const details = error.errors.map((issue) => ({
      path: issue.path.join('.'),
      message: issue.message,
    }));

    return NextResponse.json({ message: 'Bad Request.', name: 'BadRequestError', errors: details }, { status: 400 });
  }

  return NextResponse.json({ message: 'Internal Server Error.', name: 'InternalServerError' }, { status: 500 });
};
