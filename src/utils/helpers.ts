export const dropElementsToSize = <T>(list: T[], n: number): T[] => {
  const newList = [...list];
  let serializedSize = JSON.stringify(newList).length;

  while (serializedSize > n && newList.length > 0) {
    newList.pop();
    serializedSize = JSON.stringify(newList).length;
  }

  return newList;
};
