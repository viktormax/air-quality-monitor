export enum ErrorCode {
  EmailAlreadyInUse = 'EmailAlreadyInUse',
  InternalServerError = 'InternalServerError',
}

const errorCodeMapper = new Map([['auth/email-already-exists', ErrorCode.EmailAlreadyInUse]]);

export class AppError extends Error {
  code: ErrorCode;

  constructor(code: ErrorCode = ErrorCode.InternalServerError, message: string = 'Internal Server Error') {
    super(message);

    this.name = AppError.name;
    this.code = code;
  }

  toObject() {
    return {
      code: this.code,
      message: this.message,
    };
  }

  static fromError(e: unknown) {
    if (e == null || typeof e != 'object') {
      return new AppError();
    }

    if (e instanceof AppError) {
      return e;
    }

    if ('code' in e && errorCodeMapper.has(e.code as any)) {
      const { code, message } = e as Record<string, any>;
      return new AppError(errorCodeMapper.get(code), message);
    }

    return new AppError();
  }
}
