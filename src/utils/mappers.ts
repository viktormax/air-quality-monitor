import { UptimeBlockProps } from '@/components/DeviceUptime';
import { DeviceData } from '@/models/DeviceData';
import _ from 'lodash';
import moment from '@/lib/moment';
import { isDatePickerPeriod } from './typeguards';

export const mapDeviceDataToLocalizedData = (data: DeviceData[], tzName?: string) =>
  data.map((x) => ({ ...x, date: moment(x.date).tz(tzName!) }));

export const mapDeviceDataToLineChartData = (data: DeviceData[]) =>
  data.map((x) => ({
    date: moment(x.date).format('HH:mm'),
    humidity: _.round(x.humidity, 2),
    temperature: _.round(x.temperature, 2),
    light: _.round(x.light, 2),
    co2: _.round(x.co2, 2),
  }));

export const mapDeviceDataToUptimeData = (data: DeviceData[]): UptimeBlockProps[] => {
  const bars = new Map(_.range(0, 24 * 60, 10).map((minute) => [minute, 0]));
  const nowMinute = moment().hour() * 60 + moment().minute();

  data.forEach((x) => {
    const minute = x.date.hour() * 60 + x.date.minute();
    bars.set(minute, 1);
  });

  const colorMapper = new Map([
    [-1, 'gray'],
    [0, 'rose'],
    [1, 'emerald'],
  ]);

  return Array.from(bars.entries()).map(
    ([key, value]): UptimeBlockProps => ({
      color: colorMapper.get(key > nowMinute ? -1 : value),
      tooltip: `${Math.floor(key / 60)}h ${key % 60}m`,
    })
  );
};

export const mapSearchParamsToTimePeriod = (searchParams: Record<string, string | undefined>, tzName?: string) => {
  if (!tzName) {
    return {};
  }

  const { last } = searchParams;
  const now = moment().tz(tzName).startOf('minute');
  let from = now.add(-1, 'day');

  if (isDatePickerPeriod(last)) {
    from = now.add(-1, last);
  }

  return { period: { from, to: undefined } };
};
