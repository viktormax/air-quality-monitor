import { AppError, ErrorCode } from '@/utils/AppError';

interface ActionResultSuccess<T> {
  error?: undefined;
  data: T;
}

interface ActionResultFailure {
  data?: undefined;
  error: {
    code: ErrorCode;
    message: string;
  };
}

export type ActionResult<T> = ActionResultSuccess<T> | ActionResultFailure;

export const formActionResult = async <T>(func: () => Promise<T>): Promise<ActionResult<T>> => {
  try {
    return {
      data: await func(),
    };
  } catch (e) {
    return {
      error: AppError.fromError(e).toObject(),
    };
  }
};
