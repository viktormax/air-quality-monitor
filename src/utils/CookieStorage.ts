import { type ReadonlyRequestCookies } from 'next/dist/server/web/spec-extension/adapters/request-cookies';

export enum CookieStorageKey {
  tzName = 'tzName',
}

const getCookie = (name: CookieStorageKey) => {
  const key = `${name}=`;
  const decodedCookie = decodeURIComponent(document.cookie);
  const cookie = decodedCookie.split(';');
  for (let i = 0; i < cookie.length; i += 1) {
    cookie[i] = cookie[i].trimStart();
    if (cookie[i].indexOf(key) === 0) {
      return cookie[i].substring(key.length, cookie[i].length);
    }
  }
  return null;
};

const setCookie = (name: CookieStorageKey, value: string, expirationDays: number) => {
  const date = new Date();
  date.setTime(date.getTime() + expirationDays * 24 * 60 * 60 * 1000);
  const expires = `expires=${date.toUTCString()}`;
  document.cookie = `${name}=${value};${expires};path=/`;
};

const createProperty = <T>(name: CookieStorageKey) => ({
  get(cookies?: ReadonlyRequestCookies): T | undefined {
    const item = typeof window === 'undefined' ? cookies?.get(name)?.value : getCookie(name);
    return item ? (JSON.parse(item) as T) : undefined;
  },
  set(newValue: T | undefined) {
    if (typeof window === 'undefined') {
      return;
    }

    if (newValue === undefined) {
      setCookie(name, '', 0);
    } else {
      setCookie(name, JSON.stringify(newValue), 365);
    }
  },
});

export const CookieStorage = {
  tzName: createProperty<string>(CookieStorageKey.tzName),
};
