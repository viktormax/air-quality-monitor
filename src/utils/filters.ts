import moment, { Moment } from '@/lib/moment';

export const filterByDate =
  (from: Moment | undefined, to: Moment | undefined, granularity?: moment.unitOfTime.StartOf) =>
  (x: { date: Moment }) => {
    const date = moment(x.date);

    if (from && date.isBefore(from, granularity)) {
      return false;
    }

    if (to && date.isSameOrAfter(to, granularity)) {
      return false;
    }

    return true;
  };
