// TODO: separate
const datePickerPeriods = new Set(['month', 'week', 'day'] as const);
export type DatePickerPeriod = typeof datePickerPeriods extends Set<infer T> ? T : never;

export const isDatePickerPeriod = (x: any): x is DatePickerPeriod => datePickerPeriods.has(x);

type FirebaseMessageError = 'NOT_FOUND';

export const isFirestoreError = (x: unknown, messageError: FirebaseMessageError): boolean => {
  if (x == null || typeof x != 'object') {
    return false;
  }

  if (!('message' in x) || typeof x.message != 'string') {
    return false;
  }

  return x.message.includes(messageError);
};
