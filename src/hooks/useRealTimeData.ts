import { DeviceData } from '@/models/DeviceData';
import { Uuid } from '@/models/Uuid';
import { DeviceServiceClient } from '@/services/client';
import { useEffect, useState } from 'react';

const useRealTimeData = (deviceId: Uuid) => {
  const [deviceService] = useState(() => new DeviceServiceClient());
  const [data, setData] = useState<DeviceData>();

  useEffect(
    () =>
      deviceService.watchRealtimeData({
        deviceId,
        onChange: (value) => setData(value),
        // eslint-disable-next-line no-console
        onError: (error) => console.error(error), // TODO: add alert
      }),
    [deviceId, deviceService]
  );

  return data;
};

export default useRealTimeData;
