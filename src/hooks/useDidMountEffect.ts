import { useLayoutEffect, useRef } from 'react';

const useDidMountEffect = (callback: Function) => {
  const initialRenderRef = useRef(true);

  useLayoutEffect(() => {
    if (initialRenderRef.current) {
      callback();
      initialRenderRef.current = false;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};

export default useDidMountEffect;
