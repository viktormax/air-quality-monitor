import _ from 'lodash';
import { useEffect, useState } from 'react';

const useAnimatedValue = (target: number, stepDeltaMs: number, step: number = 1, initial: number = 0) => {
  const [value, setValue] = useState(initial);

  useEffect(() => {
    const int = setInterval(() => {
      setValue((prev) => {
        if (prev === target) {
          clearInterval(int);
        }

        return prev + _.clamp(target - prev, -step, step);
      });
    }, stepDeltaMs);

    return () => clearInterval(int);
  }, [step, stepDeltaMs, target]);

  return value;
};

export default useAnimatedValue;
