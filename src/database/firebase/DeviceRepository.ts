import { firestore } from 'firebase-admin';
import moment from '@/lib/moment';
import { TimePeriod } from '@/models/TimePeriod';
import { CreateDeviceData, DeviceData } from '@/models/DeviceData';
import { Uuid } from '@/models/Uuid';
import { isFirestoreError } from '@/utils/typeguards';
import { Command } from '@/models/Command';
import { Device } from '@/models/Device';
import { z } from 'zod';
import { getAdminApp } from './getAdminApp';

export class DeviceRepository {
  constructor(
    private app = getAdminApp(),
    private fs = app.firestore(),
    private db = app.database()
  ) {}

  async getDeviceData(deviceId: Uuid, period: TimePeriod): Promise<DeviceData[]> {
    let query: firestore.Query = this.fs.collection('devices').doc(deviceId).collection('data');

    if (period.from) {
      const fromDay = period.from.clone().utc().startOf('day').toDate();
      query = query.where('date', '>=', fromDay);
    }

    if (period.to) {
      const toDay = period.to.clone().utc().startOf('day').toDate();
      query = query.where('date', '<=', toDay);
    }

    const snapshot = await query.get();

    const result: DeviceData[] = [];
    snapshot.forEach((doc) => {
      const day = doc.data();
      const dayDate = day.date.toDate() as Date;

      Object.entries(day).forEach(([key, value]: [string, any]) => {
        if (Number.isNaN(Number(key))) {
          return;
        }

        const count = value[0];
        const minute = Number(key) * 10;

        result.push(
          DeviceData.parse({
            co2: value[1] / count,
            temperature: value[2] / count,
            humidity: value[3] / count,
            light: value[4] / count,
            date: moment.utc(dayDate).add(minute, 'minutes'),
          })
        );
      });
    });

    return result;
  }

  async storeDeviceData(
    deviceId: Uuid,
    day: string,
    minute: string,
    data: CreateDeviceData
  ): Promise<firestore.WriteResult> {
    const dataRef = this.fs.collection('devices').doc(deviceId).collection('data').doc(day);

    try {
      return await dataRef.update({
        [`${minute}.0`]: firestore.FieldValue.increment(1),
        [`${minute}.1`]: firestore.FieldValue.increment(data.co2),
        [`${minute}.2`]: firestore.FieldValue.increment(data.temperature),
        [`${minute}.3`]: firestore.FieldValue.increment(data.humidity),
        [`${minute}.4`]: firestore.FieldValue.increment(data.light),
      });
    } catch (err) {
      if (isFirestoreError(err, 'NOT_FOUND')) {
        return await dataRef.set({
          [minute]: {
            0: 1,
            1: data.co2,
            2: data.temperature,
            3: data.humidity,
            4: data.light,
          },
          date: firestore.Timestamp.fromDate(moment.utc(day).toDate()),
        });
      }

      throw err;
    }
  }

  async storeRealTimeData(deviceId: Uuid, data: CreateDeviceData) {
    const deviceRef = this.db.ref(`devices/${deviceId}/data`);

    await deviceRef.set({
      ...data,
      date: firestore.Timestamp.now(),
    });
  }

  async getRealTimeData(deviceId: Uuid) {
    const deviceRef = this.db.ref(`devices/${deviceId}/data`);
    const data = await deviceRef.get();
    return data.exists() ? DeviceData.parse(data.val()) : undefined;
  }

  async getDeviceUnfinishedCommands(deviceId: Uuid): Promise<Command[]> {
    const commands = await this.db.ref(`devices/${deviceId}/commands`).get();
    const now = moment.utc();

    if (!commands.exists()) {
      return [];
    }

    return z.array(Command).parse(
      Object.entries(commands.val() as object).map(([key, value]) => ({
        ...value,
        id: key,
        status: moment.utc(value.date).isBefore(now) ? 'pending' : 'scheduled',
      }))
    );
  }

  async getDevice(deviceId: Uuid): Promise<Device | undefined> {
    const deviceRef = this.fs.collection('devices').doc(deviceId);

    const doc = await deviceRef.get();
    return doc.exists ? Device.parse({ id: deviceId, ...doc.data() }) : undefined;
  }
}
