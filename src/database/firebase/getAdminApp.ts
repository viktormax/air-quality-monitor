import admin from 'firebase-admin';
import { Config } from 'sst/node/config';

export const getAdminApp = () => {
  if (admin.apps.length > 0) {
    return admin.app();
  }

  const cert = admin.credential.cert({
    projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
    clientEmail: Config.FIREBASE_CLIENT_EMAIL,
    privateKey: atob(Config.FIREBASE_PRIVATE_KEY).replace(/\\n/g, '\n'),
  });

  return admin.initializeApp({
    credential: cert,
    databaseURL: process.env.NEXT_PUBLIC_FIREBASE_DATABASE_URL,
  });
};
