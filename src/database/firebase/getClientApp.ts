import { getApp, getApps, initializeApp } from 'firebase/app';

export const getClientApp = () => {
  if (getApps().length > 0) {
    return getApp();
  }

  const firebaseConfig = {
    databaseURL: process.env.NEXT_PUBLIC_FIREBASE_DATABASE_URL,
  };

  return initializeApp(firebaseConfig);
};
