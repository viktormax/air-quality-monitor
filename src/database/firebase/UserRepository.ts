import { CreateUser, User } from '@/models/User';
import { getAdminApp } from './getAdminApp';

export class UserRepository {
  constructor(
    private app = getAdminApp(),
    private auth = app.auth()
  ) {}

  async createUser(createUser: CreateUser): Promise<User> {
    const user = await this.auth.createUser({
      email: createUser.email,
      password: createUser.password,
    });

    return User.parse(user);
  }
}
