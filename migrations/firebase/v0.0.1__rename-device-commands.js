/** @param { import('fireway').MigrateOptions } */
module.exports.migrate = async ({ firestore }) => {
  const batch = firestore.batch();
  const devicesCollection = await firestore.collection('devices').get();

  devicesCollection.forEach((deviceDoc) => {
    const deviceRef = firestore.collection('devices').doc(deviceDoc.id);
    const deviceData = deviceDoc.data();

    const commands = (deviceData.executedCommands || deviceData.commands)?.map((x) => ({
      ...x,
      date: x.dateTime,
    }));

    commands?.forEach((x) => {
      delete x.dateTime;
    });

    delete deviceData.executedCommands;

    const updatedData = { ...deviceData, commands };
    batch.set(deviceRef, updatedData);
  });

  await batch.commit();
};
