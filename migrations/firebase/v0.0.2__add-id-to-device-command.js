const uuid = require('uuid');

/** @param { import('fireway').MigrateOptions } */
module.exports.migrate = async ({ firestore }) => {
  const batch = firestore.batch();
  const devicesCollection = await firestore.collection('devices').get();

  devicesCollection.forEach((deviceDoc) => {
    const deviceRef = firestore.collection('devices').doc(deviceDoc.id);
    const deviceData = deviceDoc.data();

    deviceData.commands?.forEach((command) => {
      command.id = uuid.v4();
    });

    batch.update(deviceRef, { commands: deviceData.commands });
  });

  await batch.commit();
};
