/** @type {import('eslint').Linter.Config} */
module.exports = {
  extends: ['airbnb', 'airbnb-typescript', 'react-app', 'next', 'plugin:tailwindcss/recommended', 'prettier'],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    // Disable in order to user react-hook-form
    'react/jsx-props-no-spreading': 'off',
    // Allow reexport default
    'no-restricted-exports': 'off',
    // Deprecated starting from React 19
    'react/require-default-props': 'off',
    'import/no-extraneous-dependencies': ['error', { devDependencies: ['**/*.config.ts', '**/*.spec.ts'] }],
    // Not recommended in new TypeScript projects
    '@typescript-eslint/no-redeclare': 'off',
    // Default exports should not be enforced
    'import/prefer-default-export': 'off',
    // Enforce arrow function components
    'react/function-component-definition': ['error', { namedComponents: 'arrow-function' }],
    'no-restricted-imports': [
      'error',
      {
        patterns: [
          {
            // Disallow relative imports
            group: ['../*'],
            message: 'Usage of relative imports is not allowed.',
          },
        ],
      },
    ],
  },
};
